#### Code for handling new data == Code we used to produce segmentation with current data.

Location: https://bitbucket.org/marcthib/rtog/src/master/preprocessing/

Input: patients' folders; each patient has T1post and FLAIR; for each we have DICOMS. 



- Converting to NIFTY

  Location: [dcm2nii_rtog.py](https://bitbucket.org/marcthib/rtog/src/master/preprocessing/dcm2nii_rtog.py)

  Input: Location with the input architecture as specified above, in **folder_dcm_input**

  Ouput: NIFTY images for each patient, in **folder_nii_output**



- Preprocess images

  Location: [preprocess_images.py](https://bitbucket.org/marcthib/rtog/src/master/preprocessing/preprocess_images.py)

  Input: the **all_patients** variable lists the patients we use, and the path. 

  Output: the preprocessed NIFTY files, under 

  - */local-scratch/marcthib_scratch/rtog_proc/out0000/patient/t1post.nii*
  - */local-scratch/marcthib_scratch/rtog_proc/out0000/patient/flair.nii*



- Recovering architecture

  Location: [move_preprocessed_nifty.py](https://bitbucket.org/marcthib/rtog/src/master/preprocessing/move_preprocessed_nifty.py)

  Input: Previous architecture, under variable **preproc_output_path**

  Output: Better architecture of NIFTY files, under **moveto_path**

  Under *moveto_path/patient/t1post_proc.nii*



- Segmentation 

  Location: (different repo) [segmentation-rtog.py](https://bitbucket.org/gevaertlab/romain/src/master/segmentation-rtog.py)

  Input: Previous output path (moveto_path), in **in_folder**

  Ouput: Output segmentation maps and MP4, in **out_folder**



- Archive creation for radiomics

  Location: [aggregate_radiomics_archive.py](https://bitbucket.org/marcthib/rtog/src/master/preprocessing/aggregate_radiomics_archive.py)

  Input: Previous path, in **segmentation_raw_path**

  Ouput: Path of the folder which will contain the archive, in **out_zip_root**; as well as the tumor region we want to segment (among "core", "full", "edema", "necrosis", "enhancing"), as an argument to the script. 

  Produces *edema-segmentation.zip*. 

  


Currently, the segmentation output is on Stanford Medicine Box, under [https://stanfordmedicine.box.com/s/77j026ux99pw4d8quc04h63nr480tgf7](https://stanfordmedicine.box.com/s/77j026ux99pw4d8quc04h63nr480tgf7)